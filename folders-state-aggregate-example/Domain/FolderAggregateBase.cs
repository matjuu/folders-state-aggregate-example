﻿using System;
using System.Threading.Tasks;

namespace folders_state_aggregate_example
{
    public abstract class FolderAggregateBase : AggregateRoot
    {
        protected FolderAggregateBase(Guid id, FolderState state) : base(id)
        {
            State = state;
        }

        public FolderState State { get; private set; }

        public async Task AssignAsset(Guid assetId)
        {
            State.Assets.Add(assetId);
        }
    }
}