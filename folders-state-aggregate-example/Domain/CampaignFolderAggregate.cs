﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace folders_state_aggregate_example
{
    public class CampaignFolderAggregate : FolderAggregateBase
    {
        public CampaignFolderAggregate(Guid id, FolderState state) : base(id, state)
        {
            if (State.FolderType != FolderType.Campaign) throw new Exception("Cannot create such an aggregate");
        }

        public static CampaignFolderAggregate Create(string name)
        {
            var aggregate = new CampaignFolderAggregate(Guid.NewGuid(),
                new FolderState
                {
                    Advertiser = 0,
                    Assets = new List<Guid>(),
                    Campaign = 0,
                    Name = name,
                    ParentId = Guid.NewGuid(),
                    FolderType = FolderType.Custom
                });

            return aggregate;
        }

        public void Rename(string name)
        {
            State.Name = name + "This is totally different from the rename in CustomFolderAggregate, totally.";
        }
    }
}