﻿using System;
using System.Collections.Generic;

namespace folders_state_aggregate_example
{
    public class FolderState
    {
        public Guid ParentId { get; set; }
        public List<Guid> Assets { get; set; }
        public int Advertiser { get; set; }
        public int Campaign { get; set; }
        public string Name { get; set; }
        public FolderType FolderType { get; set; }
    }

    public enum FolderType
    {
        Custom,
        Campaign,
        Auto
    }
}