﻿using System;

namespace folders_state_aggregate_example
{
    public interface IAggregateRoot
    {
        Guid Id { get; set; }
        
    }
}