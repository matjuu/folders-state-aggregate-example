﻿using System;

namespace folders_state_aggregate_example
{
    public abstract class AggregateRoot : IAggregateRoot
    {
        protected AggregateRoot(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}