﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace folders_state_aggregate_example
{
    public class CustomFolderAggregate : FolderAggregateBase
    {
        public CustomFolderAggregate(Guid id, FolderState state) : base (id, state)
        {
            if(State.FolderType != FolderType.Custom) throw new Exception("Cannot create such an aggregate");
        }
        public static CustomFolderAggregate Create(string name)
        {
            var aggregate = new CustomFolderAggregate(Guid.NewGuid(),
                new FolderState
                {
                    Advertiser = 0,
                    Assets = new List<Guid>(),
                    Name = name,
                    ParentId = Guid.NewGuid(),
                    FolderType = FolderType.Custom
                });

            return aggregate;
        }

        public void Rename(string name)
        {
            State.Name = name;
        }
    }
}