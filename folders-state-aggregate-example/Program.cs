﻿using System;
using System.Threading.Tasks;
using folders_state_aggregate_example.Infrastructure;

namespace folders_state_aggregate_example
{
    class Program
    {
        static void Main(string[] args)
        {
            var repo = new CampaignFolderRepository();

            var aggregate = repo.Get(Guid.NewGuid());

             aggregate.Rename("ayy lmao");
        }
    }
}
