﻿using System;
using System.Collections.Generic;

namespace folders_state_aggregate_example.Infrastructure
{
    public abstract class FolderAggregateRepositoryBase
    {
        protected FolderState GetFolderState(Guid id)
        {
            return new FolderState
            {
                Assets = new List<Guid>(),
                Campaign = 0,
                Name = "test",
                Advertiser = 0,
                ParentId = Guid.NewGuid()
            };
        }
        protected FolderState GetFolderStateByCampaignId(int id)
        {
            return new FolderState
            {
                Assets = new List<Guid>(),
                Campaign = id,
                Name = "test",
                Advertiser = 0,
                ParentId = Guid.NewGuid()
            };
        }
        protected FolderState GetFolderStateByAdvertiserId(int id)
        {
            return new FolderState
            {
                Assets = new List<Guid>(),
                Campaign = 0,
                Name = "test",
                Advertiser = id,
                ParentId = Guid.NewGuid()
            };
        }
    }
}
