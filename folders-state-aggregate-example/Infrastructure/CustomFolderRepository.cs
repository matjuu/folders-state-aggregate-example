﻿using System;

namespace folders_state_aggregate_example.Infrastructure
{
    public class CustomFolderRepository : FolderAggregateRepositoryBase, IRepository<CustomFolderAggregate>
    {
        public CustomFolderAggregate Get(Guid id)
        {
            var state = GetFolderState(id);

            var aggregate = new CustomFolderAggregate(id, state);

            return state.FolderType == FolderType.Campaign ? aggregate : null;
        }

        public void Save(CustomFolderAggregate aggregate)
        {
            var state = aggregate.State;

            //Persist state to DB
        }

        public void Delete(Guid id)
        {
            //Delete from db
        }
    }
}