﻿using System;

namespace folders_state_aggregate_example.Infrastructure
{
    public interface IRepository<TModel> where TModel : IAggregateRoot
    {
        TModel Get(Guid id);
        void Save(TModel aggregate);
        void Delete(Guid id);
    }
}