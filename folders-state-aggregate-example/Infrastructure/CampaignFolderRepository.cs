﻿using System;

namespace folders_state_aggregate_example.Infrastructure
{
    public class CampaignFolderRepository : FolderAggregateRepositoryBase, IRepository<CampaignFolderAggregate>
    {
        public CampaignFolderAggregate Get(Guid id)
        {
            var state = GetFolderState(id);

            var aggregate = new CampaignFolderAggregate(id, state);

            return state.FolderType == FolderType.Campaign ? aggregate : null;
        }

        public void Save(CampaignFolderAggregate aggregate)
        {
            var state = aggregate.State;

            //Persist state to DB
        }

        public void Delete(Guid id)
        {
            //Delete from db
        }
    }
}